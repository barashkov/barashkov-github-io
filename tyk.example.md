### Создаю первое приложение appeal-ws. Доступ без ключа.
curl -v -H "x-tyk-authorization: YQmasSVIHF7s5DzW4r9R6QMWgVTufOGT" \
  -s \
  -H "Content-Type: application/json" \
  -X POST \
  -d '{
    "name": "appeal-ws",
    "slug": "appeal-ws",
    "api_id": "appeal-ws",
    "org_id": "1",
    "use_keyless": true,
    "auth": {
      "auth_header_name": "Authorization"
    },
    "definition": {
      "location": "header",
      "key": "x-api-version"
    },
    "version_data": {
      "not_versioned": true,
      "versions": {
        "Default": {
          "name": "Default",
          "use_extended_paths": true
        }
      }
    },
    "proxy": {
      "listen_path": "/appeal-ws/",
      "target_url": "http://appeal.tomcats.sa.on-dev.ru/appeal-ws/rest",
      "strip_listen_path": true
    },
    "active": true
}' http://apigw.dev.eoportal.ru/tyk/apis | python -mjson.tool

curl -H "x-tyk-authorization: YQmasSVIHF7s5DzW4r9R6QMWgVTufOGT" -s http://apigw.dev.eoportal.ru/tyk/reload/group | python -mjson.tool

### Проверяю доступ
curl -X GET http://apigw.dev.eoportal.ru/appeal-ws/

### Создаю ключ доступа для приложения appeal-ws
curl -X POST -H "x-tyk-authorization: YQmasSVIHF7s5DzW4r9R6QMWgVTufOGT" \
  -s \
  -H "Content-Type: application/json" \
  -X POST \
  -d '{
    "allowance": 1000,
    "rate": 1000,
    "per": 1,
    "expires": -1,
    "quota_max": -1,
    "org_id": "1",
    "quota_renews": 1449051461,
    "quota_remaining": -1,
    "quota_renewal_rate": 60,
    "access_rights": {
      "appeal-ws": {
        "api_id": "appeal-ws",
        "api_name": "appeal-ws",
        "versions": ["Default"]
      }
    },
    "meta_data": {}
  }' http://apigw.dev.eoportal.ru/tyk/keys/create | python -mjson.tool

Ответ:
{
    "key": "10df4e41c09a74bcea321b027302591e1",
    "status": "ok",
    "action": "added",
    "key_hash": "bc7418e0"
}

### Меняю приложение appeal-ws. Включаю доступ по ключу ("use_keyless": false).
curl -v -H "x-tyk-authorization: YQmasSVIHF7s5DzW4r9R6QMWgVTufOGT" \
  -s \
  -H "Content-Type: application/json" \
  -X PUT \
  -d '{
    "name": "appeal-ws",
    "slug": "appeal-ws",
    "api_id": "appeal-ws",
    "org_id": "1",
    "use_keyless": false,
    "auth": {
      "auth_header_name": "Authorization"
    },
    "definition": {
      "location": "header",
      "key": "x-api-version"
    },
    "version_data": {
      "not_versioned": true,
      "versions": {
        "Default": {
          "name": "Default",
          "use_extended_paths": true
        }
      }
    },
    "proxy": {
      "listen_path": "/appeal-ws/",
      "target_url": "http://appeal.tomcats.sa.on-dev.ru/appeal-ws/rest",
      "strip_listen_path": true
    },
    "active": true
}' http://apigw.dev.eoportal.ru/tyk/apis/appeal-ws/ | python -mjson.tool

curl -H "x-tyk-authorization: YQmasSVIHF7s5DzW4r9R6QMWgVTufOGT" -s http://apigw.dev.eoportal.ru/tyk/reload/group | python -mjson.tool

### Проверяю, что нет доступа без ключа и есть доступ с ключем.
curl -X GET http://apigw.dev.eoportal.ru/appeal-ws/

curl -X GET -H "Authorization: 10df4e41c09a74bcea321b027302591e1" http://apigw.dev.eoportal.ru/appeal-ws/

### Добавляю второе приложение. Доступно по ключу.
curl -v -H "x-tyk-authorization: YQmasSVIHF7s5DzW4r9R6QMWgVTufOGT" \
  -s \
  -H "Content-Type: application/json" \
  -X POST \
  -d '{
    "name": "orders-ws",
    "slug": "orders-ws",
    "api_id": "orders-ws",
    "org_id": "1",
    "use_keyless": false,
    "auth": {
      "auth_header_name": "Authorization"
    },
    "definition": {
      "location": "header",
      "key": "x-api-version"
    },
    "version_data": {
      "not_versioned": true,
      "versions": {
        "Default": {
          "name": "Default",
          "use_extended_paths": true
        }
      }
    },
    "proxy": {
      "listen_path": "/orders-ws/",
      "target_url": "http://orders.tomcats.sa.on-dev.ru/orders-ws/rest",
      "strip_listen_path": true
    },
    "active": true
}' http://apigw.dev.eoportal.ru/tyk/apis | python -mjson.tool

curl -H "x-tyk-authorization: YQmasSVIHF7s5DzW4r9R6QMWgVTufOGT" -s http://apigw.dev.eoportal.ru/tyk/reload/group | python -mjson.tool

### Проверяю, что orders-ws не доступен без авторизации или по созданному ранее ключу
curl -X GET http://apigw.dev.eoportal.ru/orders-ws/
curl -X GET -H "Authorization: 10df4e41c09a74bcea321b027302591e1" http://apigw.dev.eoportal.ru/orders-ws/

### Модифицирую существующий ключ для разрешения доступа к orders-ws
curl X PUT -H "x-tyk-authorization: YQmasSVIHF7s5DzW4r9R6QMWgVTufOGT" \
  -s \
  -H "Content-Type: application/json" \
  -d '{
    "allowance": 1000,
    "rate": 1000,
    "per": 1,
    "expires": -1,
    "quota_max": -1,
    "org_id": "1",
    "quota_renews": 1449051461,
    "quota_remaining": -1,
    "quota_renewal_rate": 60,
    "access_rights": {
      "appeal-ws": {
        "api_id": "appeal-ws",
        "api_name": "appeal-ws",
        "versions": ["Default"]
      },
      "orders-ws": {
        "api_id": "orders-ws",
        "api_name": "orders-ws",
        "versions": ["Default"]
      }
    },
    "meta_data": {}
  }' http://apigw.dev.eoportal.ru/tyk/keys/10df4e41c09a74bcea321b027302591e1 | python -mjson.tool

### Проверяю что оба приложения доступны по ключу
curl -X GET -H "Authorization: 10df4e41c09a74bcea321b027302591e1" http://apigw.dev.eoportal.ru/appeal-ws/

curl -X GET -H "Authorization: 10df4e41c09a74bcea321b027302591e1" http://apigw.dev.eoportal.ru/orders-ws/

### Примечание
Для установки даты окончания действия ключа используется поле "expires". Это метка времени Unix, которая показывает секунды с эпохи (1 января 1970 года).
Документация: https://tyk.io/docs/tyk-gateway-api/
