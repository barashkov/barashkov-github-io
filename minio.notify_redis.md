## Minio. Настройка отправки событий в redis

### Предварительные настройки

#### Установить minio клиент (mc)
```
curl https://dl.min.io/client/mc/release/linux-amd64/mc --create-dirs -o $HOME/minio-binaries/mc
chmod +x $HOME/minio-binaries/mc
export PATH=$PATH:$HOME/minio-binaries/
```

#### Настроить alias для mc
`mc alias set s3dev https://s3dev.dzz.eoportal.ru minio miniopassword`

#### Установить redis клиента
`sudo apt-get install redis-tools`

### Включение сбора событий для бакета

#### Добавить в minio точку подключения redis
`mc admin config set s3dev notify_redis:{{notify_mountpoint}} format=access address=redis:6379 key={{redis_key}} password=password`
> где   
{{notify_mountpoint}} - имя точки подключения для событий redis в minio   
{{redis_key}} - имя ключа в redis

Пример:
```
mc admin config set s3dev notify_redis:events format=access address=redis:6379 key=minio_events password=password
mc admin service restart s3dev
```

#### Включить сбор событий для бакета {{backet_name}}
`mc event add s3dev/{{backet_name}} arn:minio:sqs::{{notify_mountpoint}}:redis`
> где {{backet_name}} - имя бакета для которого собираем события

Пример:   
`mc event add s3dev/backup arn:minio:sqs::events:redis`

### Посмотреть события в redis
Примеры:   
```
redis-cli -h 10.110.0.126 -a password lindex sat-events -1
redis-cli -h 10.110.0.126 -a password lrange sat-events -3 -1
redis-cli -h 10.110.0.126 -a password lrange sat-events 0 1000
```
### Прочие полезные команды

#### Посмотреть настройки notify_redis
`mc admin config get s3dev notify_redis`

#### Посмотреть настроенные ARN
`mc admin info --json s3dev |jq .info.sqsARN`

#### Удалить в minio точку подключения redis
```
mc admin config reset s3dev notify_redis:events
mc admin service restart s3dev
```

#### Посмотреть включенные уведомления для бакета backup
`mc event ls s3dev/backup`

#### Выключить уведомления для бакета backup
`mc event rm s3dev/backup arn:minio:sqs::events:redis`

#### Включить уведомление для бакета с параметрами фильтров: типы операций put,delete,get
`mc event add s3dev/backup arn:minio:sqs::events:redis --event put,delete,get`

#### Включить уведомление для бакета с параметрами префиксного фильтра, чтобы получать уведомления только при добавлении файлов в определенную папку (например, photos/).
`mc event add s3dev/backup arn:minio:sqs::events:redis --prefix photos/`

#### Включить уведомление для бакета с параметрами суффиксного фильтра, чтобы получать уведомления только при добавлении файлов с расширением .jpg
`mc event add s3dev/backup arn:minio:sqs::events:redis --suffix .jpg`

#### Включить уведомление для бакета с игнорированием повторяющиеся уведомления (флаг -p)
`mc event add s3dev/backup arn:minio:sqs::events:redis -p`

