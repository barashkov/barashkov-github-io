# barashkov.github.io

## Приложения и сервисы

[BorgBackup](borgbackup.md) - Резервное копирование (+дедупликация +шифрование +просто)

## Разные заметки

[Minio. Настройка отправки событий в redis](minio.notify_redis.md)

[Использования API Gateway для организации доступа к сервисам на основе долго жувущих ключей](tyk.example.md)

[Сменить домен для artifactory v5.6 не зная пароля admin](artifactory.password.change.md)

[Установка Project 2016 на SharePoint в изолированном режиме на одном сервере для совместной работы](project2016.sharepoint.install.md)

[Установка kafka и cassandra через docker compose](docker.kafka.cassandra.md)

[Восстановление postgres после сбоя](postgres.error.resetwal.md)

[Передача файлов через DVB](dvb.data.stream.md)
