## Передача файлов через DVB 

### Задача

Требуется передавать данные в потоках вещания DVB-S2 на трансиверы с односторонним доступом.

### Реализация

Предлагается использовать встроенный в MPEG протокол DSM-CC

### Кодирование данных

Установить opencaster (в репозитариях только Ubuntu 20.04 и Ubuntu 18.04)
```
sudo apt install -y opencaster
wget https://github.com/aventuri/opencaster/raw/master/code/tools/oc2sec/file2mod.py
sudo mv file2mod.py /usr/local/bin/
wget https://github.com/aventuri/opencaster/blob/master/code/tools/oc2sec/mod2sec.py
sudo mv mod2sec.py /usr/local/bin/
wget https://github.com/aventuri/opencaster/raw/master/code/tools/oc2sec/oc-update.sh
chmod +x oc-update.sh

```
В папку bluemarble кладем файлы, которые нужно отправить в карусели. Формируем файл на отправку bluemarble.ts:
```
./oc-update.sh bluemarble 0xB 5 2003 7 0 0 1 4066 0 2
```
Смешиваем любой видео файл в формате ts с потоком данных:
```
tscbrmuxer b:100000 space-timelaps-720p.ts c:10000 bluemarble.ts > muxed.ts
```
Файл muxed.ts готов для трансляции через DVB-S2.

### Инструменты анализа ts потоков

TSReader 2.8.47c
http://forum.ru-board.com/forum.cgi?action=filter&forum=35&filterby=topictitle&word=TSReader

### Декодирование данных

 https://github.com/NoordGuy/libtssipython

### Источники

 https://webstore.iec.ch/preview/info_isoiec13818-6%7Bed1.0%7Den.pdf

 http://forum.doom9.org/showthread.php?t=155303

 https://files.stroyinf.ru/Data/491/49124.pdf

 https://www.etsi.org/deliver/etsi_tr/101200_101299/101202/01.02.01_60/tr_101202v010201p.pdf

 https://cdn.standards.iteh.ai/samples/83239/4973f67a57114579827b27c0b5624084/ISO-IEC-13818-1-2022.pdf

 http://www.telemidia.puc-rio.br/~rafaeldiniz/public_files/normas/ISO-13818/iso13818-1/ISO_IEC_13818-1_2007_PDF_version_(en).pdf
 
 http://www.telemidia.puc-rio.br/~rafaeldiniz/public_files/normas/ISO-13818/iso13818-6/ISO_IEC_13818-6_1998_PDF_version_(en).pdf

### Примечание
Это не законченная статья...
