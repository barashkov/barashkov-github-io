## Установка kafka и cassandra через docker compose

```
version: "3"

services:

  zookeeper:
    image: zookeeper
    restart: always
    mem_limit: 512m
    container_name: zookeeper
    hostname: zookeeper
    ports:
      - 2181:2181
    environment:
      ZOO_MY_ID: 1

  broker:
    container_name: broker
    image: wurstmeister/kafka
    restart: always
    mem_limit: 2g
    ports:
      - 9092:9092
    environment:
      KAFKA_ADVERTISED_HOST_NAME: broker
      KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
      KAFKA_MESSAGE_MAX_BYTES: 2000000
      # Время хранения очередей в минутах (default:10080)
      KAFKA_OFFSETS_RETANTION_MINUTES: 15

  kafka_manager:
    image: hlebalbau/kafka-manager:stable
    container_name: kafka-manager
    restart: always
    mem_limit: 256m
    ports:
      - 9000:9000
    environment:
      ZK_HOSTS: "zookeeper:2181"
      APPLICATION_SECRET: "adminadmin"
      KAFKA_MANAGER_AUTH_ENABLED: "true"
      KAFKA_MANAGER_USERNAME: admin
      KAFKA_MANAGER_PASSWORD: adminadmin
    command: -Dpidfile.path=/dev/null

  cassandradb:
    container_name: cassandra
    hostname: cassandra
    image: cassandra
    mem_limit: 2g
    restart: always
    ports:
      - 7000:7000
      - 9042:9042
    volumes:
      - $PWD/cassandra/data:/var/lib/cassandra
```
SOURCES: 

https://medium.com/sfu-cspmp/building-data-pipeline-kafka-docker-4d2a6cfc92ca

https://habr.com/ru/company/southbridge/blog/550934/

https://habr.com/ru/post/155115/
